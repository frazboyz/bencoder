package org.pixie.bencoding.bencode;

/**
 * Project Bencoding
 * Created by Francis on 27/03/14.
 */
public interface BElement {

    /**
     * @return the encoded value
     */
    public String encode();

}
